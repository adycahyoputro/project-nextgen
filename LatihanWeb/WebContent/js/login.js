	    $(document).ready(function() {
			$("#submit").click(function() {
				var regUsername = new RegExp("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$");
				if (!regUsername.test($('#nama').val())) {
					$("#alert").attr("style", "display: block;");
					$("#alert").html("Nama tidak boleh kosong dan harus dalam bentuk email");
					$("#nama").focus();
					return false;
				}
				var search = {};
				search["nama"] = $("#nama").val();
				search["password"] = $("#password").val();
				
   		 		$.ajax({
		    		url: "http://localhost:8080/login_api",
	 		   		contentType : "application/json",
					type:"POST",
					data: JSON.stringify(search),
					success: function (result) {
						if (result.returnCode == 0) {
							//setCookie("token", result.pengguna.token, 1);
							var token = result.pengguna.token;
			                sessionStorage.setItem("text", token);
			                window.location = "penjualan.html";
							console.log("success : ", result);
			      			//alert(result.pengguna.token);
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(result.errorMessage);
							console.log("Error : ", result);
						}
		        	},
		        	error: function (e) {
		        		$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
			        }
				 })
			})
		})