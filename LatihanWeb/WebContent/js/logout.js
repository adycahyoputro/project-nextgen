//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);

$(document).ready(function() {
	if (token == "") {
		window.location = "login.html";
	}
	$("#logout").click(function() {
		$.ajax({
			url : 'http://localhost:8080/logout_api',
			data : {},
			type : 'GET',
			headers : {
				'token' : token
			},
			error : function() {
				$("#alert").attr("style", "display: block;");
				$("#alert").html("Error log out");
			},
			success : function(data) {
				if (data.returnCode != 0) {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(data.errorMessage);
				}
				//setCookie("token", "", -1);
				//sessionStorage.clear();
				sessionStorage.removeItem("text");
				window.location = "login.html";
			},
		});
	});
})
