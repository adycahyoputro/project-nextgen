		var objBarang = "";
		// token = getCookie("token");
		var token = sessionStorage.getItem("text", token);

		$(document).ready(function() {
			$.ajax({
	    		url: "http://localhost:8080/getallkategoripengguna",
				type:"GET",
				data: {

				},
				success: function (data) {
					objBarang = data;
					if (data.returnCode == 0) {
						getKategoriPengguna();
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(objBarang.errorMessage);
					}
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
			$("#submit").click(function() {
				var regUsername = new RegExp("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$");
	    		if (!regUsername.test($('#nama').val())) {
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").html("Email pengguna tidak valid");
	    			$("#nama").focus();
	    			return false;
	    		}
		        var regPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%\^&\*])(?=.{8,})");
		        if (!regPassword.test($("#password").val())) {
		        	$("#alert").attr("style", "display: block;");
		        	$("#alert").html("Password tidak valid, panjang minimal 8, harus mengandung huruf besar, angka dan simbol");
		        	$("#password").focus();
		        	return false;
		        }	
		        if ($("#password").val() != $("#konfirmasiPassword").val()) {
		        	$("#alert").attr("style", "display: block;");
		        	$("#alert").html("Konfirmasi password tidak sama dengan password");
		        	$("#konfirmasiPassword").focus();
		        	return false;
		        }	    		
	    		if($("#kategoriPengguna").val() == ""){
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").text("Kategori pengguna harus dipilih");
	    			$("#kategoriPengguna").focus();
	    			return false;
	    		}

				var pengguna = {
						password : $("#password").val(),
						kategoriPengguna : {
							id : $("#kategoriPengguna option:selected").val()
						},
						id : 0,
						nama : $("#nama").val()
					};

				$.ajax({
					url : "http://localhost:8080/addpengguna",
					contentType : "application/json",
					type : "POST",
					headers : {
						'token' : token
					},
					data : JSON.stringify(pengguna),
					success : function(result) {
						if (result.returnCode == 0) {
							//$("#alert").attr("style", "display: block;");
							//$("#alert").html("Tambah pengguna berhasil");
							window.location = "pengguna.html";
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(result.errorMessage);
						}
					},
					error : function(e) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
					}
				})
			})
		})
		
		function getKategoriPengguna() {
			var option = "<option value=''>Pilih Tipe Pengguna</option>";
			jQuery.each(objBarang.kategoriPengguna, function(key, value) {
				option += "<option value="+value.id+">"+value.nama+"</option>";
			});
			$("#kategoriPengguna").append(option);
		}