var object = "";
//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);
$(document).ready(function() {
	$.ajax({
		url : "http://localhost:8080/getallsupplier",
		type : "GET",
		data : {

		},
		success : function(data) {
			object = data;
			if (data.returnCode == 0) {
				getAllSupplier();
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(object.errorMessage);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
		}
	})
})
function getAllSupplier() {
	var row = [];
	jQuery
			.each(
					object.supplier,
					function(index, value) {
						row += '<tr>' + '<td>'
								+ value.id
								+ '</td>'
								+ '<td>'
								+ value.nama
								+ '</td>'
								+ '<td>'
								+ value.tanggalBuat
								+ '</td>'
								+ '<td>'
								+ value.tanggalEdit
								+ '</td>'
								+ '<td>'
								+ value.alamat
								+ '</td>'
								+ '<td>'
								+ value.noTelp
								+ '</td>'
								+ '<td align="center"> <a href="edit_supplier.html?id='
								+ value.id
								+ '" class="btn btn-sm btn-warning" title="Edit"> <i class="far fa-edit"></i></a> </td>'
								+ '<td> <a href="#" class="btn btn-sm btn-danger" title="Hapus" onclick="hapus('
								+ value.id
								+ ')"><i class="fas fa-trash-alt"></i></a> </td>'
								+ '</tr>';
					});
	$("table tbody").append(row);
}
function hapus(id) {
	var answer = confirm('Yakin menghapus data supplier ?');
	if (answer) {
		$.ajax({
			url : "http://localhost:8080/deletesupplier?id=" + id,
			type : "GET",
			headers : {
				'token' : token
			},
			success : function(data) {
				if (data.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Hapus supplier berhasil");
					window.location = "supplier.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(data.errorMessage);
					console.log("error", data);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
				console.log("error", e.responseText);
			}
		})
	} else {
		event.preventDefault();
	}
}