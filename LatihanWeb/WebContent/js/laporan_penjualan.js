//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);

$("#tampilkan").click(
		function() {
			$("#alert").attr("style", "display: none;");
			$("#table tbody").remove();
			$("#table").append("<tbody></tbody>");
			$.ajax({
				url : 'http://localhost:8080/laporan/penjualan?start='
						+ $("#tglawal").val() + '&end=' + $("#tglakhir").val(),
				data : {

				},
				headers : {
					'token' : token
				},
				error : function() {
					$("#alert").attr("style", "display: block;");
					$("#alert").html("Error koneksi mengambil data laporan");
				},
				success : function(data) {
					obj = data;
					if (obj.returnCode != 0) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(obj.errorMessage);
					} else {
						var rows = "";
						for (var i = 0; i < obj.penjualan.length; ++i) {
							rows = rows + "<tr>" + "<td>" + (i + 1) + "</td>"
									+ "<td>" + obj.penjualan[i].barang
									+ "</td>" + "<td>"
									+ obj.penjualan[i].tanggal + "</td>"
									+ "<td>" + obj.penjualan[i].hargaBeli
									+ "</td>" + "<td>"
									+ obj.penjualan[i].hargaJual + "</td>"
									+ "<td>" + obj.penjualan[i].diskon
									+ "</td>" + "<td>"
									+ obj.penjualan[i].hargaJualNet + "</td>"
									+ "<td>" + obj.penjualan[i].jumlah
									+ "</td>" + "<td>"
									+ obj.penjualan[i].totalPenjualan + "</td>"
									+ "<td>" + obj.penjualan[i].totalLaba
									+ "</td>" + "</tr>";
						}
						$("#table tbody").append(rows);
					}
				},
				type : 'GET'
			});
		});