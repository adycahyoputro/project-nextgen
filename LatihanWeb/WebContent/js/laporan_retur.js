//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);

$("#tampilkan").click(
		function() {
			$("#alert").attr("style", "display: none;");
			$("#table tbody").remove();
			$("#table").append("<tbody></tbody>");
			$.ajax({
				url : 'http://localhost:8080/laporan/retur?start='
						+ $("#tglawal").val() + '&end=' + $("#tglakhir").val(),
				data : {

				},
				headers : {
					'token' : token
				},
				error : function() {
					$("#alert").attr("style", "display: block;");
					$("#alert").html("Error koneksi mengambil data laporan");
				},
				success : function(data) {
					obj = data;
					if (obj.returnCode != 0) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(obj.errorMessage);
					} else {
						var rows = "";
						for (var i = 0; i < obj.retur.length; ++i) {
							rows = rows + "<tr>" + "<td>" + (i + 1) + "</td>"
									+ "<td>" + obj.retur[i].barang + "</td>"
									+ "<td>" + obj.retur[i].tanggal + "</td>"
									+ "<td>" + obj.retur[i].keterangan
									+ "</td>" + "<td>" + obj.retur[i].hargaJual
									+ "</td>" + "<td>" + obj.retur[i].jumlah
									+ "</td>" + "<td>" + obj.retur[i].subTotal
									+ "</td>" + "</tr>";
						}
						$("#table tbody").append(rows);
					}
				},
				type : 'GET'
			});
		});