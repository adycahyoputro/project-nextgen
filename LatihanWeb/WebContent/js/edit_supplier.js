    //var token = getCookie("token");
    var token = sessionStorage.getItem("text", token);
	var object = "";
	    $(document).ready(function() {
		    const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const id = urlParams.get('id');
	    	$.ajax({
	    		url: "http://localhost:8080/getsupplier?id="+id,
				type:"GET",
				success: function (data) {
					object = data;
					if (object.returnCode == 0) {
						getSupplier();
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(object.errorMessage);
					}
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
					console.log("error", e.responseText);
		        }
			 })
	    	$("#submit").click(function() {  		
	    		if($("#nama").val() == ""){
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").text("Nama supplier tidak boleh kosong");
	    			$("#nama").focus();
	    			return false;
	    		}
	    		if($("#alamat").val() == ""){
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").text("Alamat supplier tidak boleh kosong");
	    			$("#alamat").focus();
	    			return false;
	    		}
	    		if($("#noTelp").val() == ""){
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").text("Nomor telepon supplier tidak boleh kosong");
	    			$("#noTelp").focus();
	    			return false;
	    		}
	    		var edit = {};
	    		edit["id"] = $("#id").val();
	    		edit["nama"] = $("#nama").val();
	    		edit["alamat"] = $("#alamat").val();
	    		edit["noTelp"] = $("#noTelp").val();
	    		var textJson = JSON.stringify(edit);
	    		postEditSupplier(textJson);
			})
	    })
		function getSupplier() {
			var Supplier = object.supplier;
			$("label[for='id']").text("ID Supplier : "+Supplier.id);
			$("#id").val(Supplier.id);
			$("#nama").val(Supplier.nama);
			$("#tanggalBuat").val(Supplier.tanggalBuat);
			$("#tanggalEdit").val(Supplier.tanggalEdit);
			$("#alamat").val(Supplier.alamat);
			$("#noTelp").val(Supplier.noTelp);
		}
		function postEditSupplier(Json) {
			$.ajax({
	    		url: "http://localhost:8080/editsupplier",
 		   		contentType : "application/json",
				type:"POST",
				headers: { 'token': token },
				data: Json,
				success: function (result) {
					if (result.returnCode == 0) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html("Edit supplier berhasil");
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(result.errorMessage);
					}
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
		}