var object = "";
//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);

$(document).ready(function() {
	$.ajax({
		url : "http://localhost:8080/getallkategoribarang",
		type : "GET",
		data : {

		},
		success : function(data) {
			object = data;
			if (data.returnCode == 0) {
				getKategoriNamaBarang();
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(object.errorMessage);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
		}
	})
	$("#submit").click(function() {
		if ($("#kodeBarang").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Kode barang tidak boleh kosong");
			$("#kodeBarang").focus();
			return false;
		}
		if ($("#nama").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Nama barang tidak boleh kosong");
			$("#nama").focus();
			return false;
		}
		if ($("#satuan").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Satuan tidak boleh kosong");
			$("#satuan").focus();
			return false;
		}
		if ($("#kategori").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Kategori barang harus dipilih");
			$("#kategori").focus();
			return false;
		}

		var barang = {
			kodeBarang : $("#kodeBarang").val(),
			satuan : $("#satuan").val(),
			hargaBeli : 0,
			hargaJual : 0,
			stok : 0,
			kategori : {
				id : $("#kategori option:selected").val()
			},
			id : 0,
			nama : $("#nama").val()
		};

		$.ajax({
			url : "http://localhost:8080/addbarang",
			contentType : "application/json",
			type : "POST",
			headers : {
				'token' : token
			},
			data : JSON.stringify(barang),
			success : function(result) {
				if (result.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Tambah barang berhasil");
					window.location = "barang.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(result.errorMessage);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
			}
		})
	})
})
function getKategoriNamaBarang() {
	var option = [];
	jQuery.each(object.kategoriBarang, function(key, value) {
		option += "<option value=" + value.id + ">" + value.nama + "</option>";
	});
	$("#kategori").append(option);
}