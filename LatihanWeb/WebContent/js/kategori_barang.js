	var object = "";
	//var token = getCookie("token");
	var token = sessionStorage.getItem("text", token);
	
	    $(document).ready(function() {
   		 		$.ajax({
		    		url: "http://localhost:8080/getallkategoribarang",
					type:"GET",
					data: {

					},
					success: function (data) {
						object = data;
						if (data.returnCode == 0) {
							getAllKategoriBarang();
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(object.errorMessage);
						}
		        	},
		        	error: function (e) {
		        		$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
			        }
				 })
			})
		function getAllKategoriBarang() {
			var row = [];
	    	jQuery.each(object.kategoriBarang, function(index, value) {
		    	row += '<tr>'+
                	'<td>' + value.id +'</td>' + 
                	'<td>'+ value.nama +'</td>' +
                	'<td>'+ value.tanggalBuat +'</td>' +
                	'<td>'+ value.tanggalEdit +'</td>' +
                	'<td> <a href="edit_kategori_barang.html?id='+value.id+'" class="btn btn-sm btn-warning" title="Edit"> <i class="far fa-edit"></i></a> </td>' +
                	'<td> <a href="#" class="btn btn-sm btn-danger" title="Hapus" onclick="hapus('+value.id+')"><i class="fas fa-trash-alt"></i></a> </td>' +
            	'</tr>';
			});
	    	$("table tbody").append(row);
		}
		function hapus(id) {
			if(confirm('Yakin menghapus data kategori barang ?')==true){
				$.ajax({
		    		url: "http://localhost:8080/deletekategoribarang?id="+id,
					type:"GET",
					headers: { 'token': token },
					success: function (data) {
						if (data.returnCode == 0) {
							console.log("success :", data);
							//$("#alert").attr("style", "display: block;");
							//$("#alert").html("Hapus kategori barang berhasil");
							window.location = "kategori_barang.html";
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(data.errorMessage);
						}
		        	},
		        	error: function (e) {
		        		$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
			        }
				 })
		    }else {
				event.preventDefault();
			}
		}