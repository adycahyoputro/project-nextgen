//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);
$(document).ready(function() {
	$("#submit").click(function() {
		if ($("#nama").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Nama supplier tidak boleh kosong");
			$("#nama").focus();
			return false;
		}
		if ($("#alamat").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Alamat supplier tidak boleh kosong");
			$("#alamat").focus();
			return false;
		}
		if ($("#noTelp").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Nomor telepon supplier tidak boleh kosong");
			$("#noTelp").focus();
			return false;
		}

		var search = {};
		search["nama"] = $("#nama").val();
		search["alamat"] = $("#alamat").val();
		search["noTelp"] = $("#noTelp").val();

		$.ajax({
			url : "http://localhost:8080/addsupplier",
			contentType : "application/json",
			type : "POST",
			headers : {
				'token' : token
			},
			data : JSON.stringify(search),
			success : function(result) {
				if (result.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Tambah data supplier berhasil");
					window.location = "supplier.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(result.errorMessage);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
			}
		})
	})
})