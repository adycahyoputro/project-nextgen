//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);

$(document).ready(function() {
	$("#submit").click(function() {
		if ($("#nama").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Nama kategori barang tidak boleh kosong");
			$("#nama").focus();
			return false;
		}
		var search = {};
		search["nama"] = $("#nama").val();

		$.ajax({
			url : "http://localhost:8080/addkategoribarang",
			contentType : "application/json",
			type : "POST",
			headers : {
				'token' : token
			},
			data : JSON.stringify(search),
			success : function(result) {
				if (result.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Tambah kategori barang berhasil");
					window.location = "kategori_barang.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(result.errorMessage);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
			}
		})
	})
})