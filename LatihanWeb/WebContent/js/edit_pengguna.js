		//var token = getCookie("token");
		var token = sessionStorage.getItem("text", token);

		$(document).ready(function() {
			getKategoriPengguna();
			$("#submit").click(function() {
				var regPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%\^&\*])(?=.{8,})");
		        if (!regPassword.test($("#password").val())) {
		        	$("#alert").attr("style", "display: block;");
		        	$("#alert").html("Password tidak valid, panjang minimal 8, harus mengandung huruf besar, angka dan simbol");
		        	$("#password").focus();
		        	return false;
		        }	
		        if ($("#password").val() != $("#konfirmasiPassword").val()) {
		        	$("#alert").attr("style", "display: block;");
		        	$("#alert").html("Konfirmasi password tidak sama dengan password");
		        	$("#konfirmasiPassword").focus();
		        	return false;
		        }	    		
	    		if($("#kategoriPengguna").val() == "") {
	    			$("#alert").attr("style", "display: block;");
	    			$("#alert").text("Kategori pengguna harus dipilih");
	    			$("#kategoriPengguna").focus();
	    			return false;
	    		}

				var pengguna = {
						password : $("#password").val(),
						kategoriPengguna : {
							id : $("#kategoriPengguna option:selected").val()
						},
						id : $("#id").val(),
						nama : $("#nama").val()
					};

				$.ajax({
					url : "http://localhost:8080/editpengguna",
					contentType : "application/json",
					type : "POST",
					headers : {
						'token' : token
					},
					data : JSON.stringify(pengguna),
					success : function(result) {
						if (result.returnCode == 0) {
							$("#alert").attr("style", "display: block;");
							$("#alert").html("Edit pengguna berhasil");
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(result.errorMessage);
						}
					},
					error : function(e) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
					}
				})
			})
		})
		function getKategoriPengguna() {
			$.ajax({
	    		url: "http://localhost:8080/getallkategoripengguna",
				type:"GET",
				data: {

				},
				success: function (data) {
					if (data.returnCode == 0) {
						var option = "<option value=''>Pilih Tipe Pengguna</option>";
						jQuery.each(data.kategoriPengguna, function(key, value) {
							option += "<option value="+value.id+">"+value.nama+"</option>";
						});
						$("#kategoriPengguna").append(option);
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(data.errorMessage);
					}
					getPenggunaById();
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
		}
		function getPenggunaById() {
			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const id = urlParams.get('id');
			$.ajax({
	    		url: "http://localhost:8080/getpengguna?id="+id,
				type:"GET",
				data: {

				},
				success: function (data) {
					if (data.returnCode == 0) {
						console.log("success : ", data);
						$("label[for='id']").text("ID Pengguna : "+data.pengguna.id);
						$("#id").val(data.pengguna.id);
						$("#nama").val(data.pengguna.nama);
						$("#tanggalDibuat").val(data.pengguna.tanggalBuat);
						$("#tanggalDiedit").val(data.pengguna.tanggalEdit);
						$("select[name='kategoriPengguna']").find("option[value='"+data.pengguna.kategoriPengguna.id+"']").attr("selected",true).change();
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(data.errorMessage);
					}
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
		}
		function sleep(ms) {
			return new Promise(resolve => setTimeout(resolve, ms));
		}