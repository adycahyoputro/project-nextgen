var objBarang = "";
//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);
$(document).ready(function() {
	$.ajax({
		url : "http://localhost:8080/getallbarang",
		type : "GET",
		data : {

		},
		success : function(data) {
			objBarang = data;
			if (data.returnCode == 0) {
				getAllBarang();
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(objBarang.errorMessage);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
		}
	})
})
function getAllBarang() {
	var row = [];
	jQuery
			.each(
					objBarang.barang,
					function(index, value) {
						row += '<tr>' + '<td>'
								+ value.id
								+ '</td>'
								+ '<td>'
								+ value.kodeBarang
								+ '</td>'
								+ '<td>'
								+ value.tanggalBuat
								+ '</td>'
								+ '<td>'
								+ value.tanggalEdit
								+ '</td>'
								+ '<td>'
								+ value.nama
								+ '</td>'
								+ '<td>'
								+ value.hargaBeli
								+ '</td>'
								+ '<td>'
								+ value.hargaJual
								+ '</td>'
								+ '<td>'
								+ value.satuan
								+ '</td>'
								+ '<td>'
								+ value.stok
								+ '</td>'
								+ '<td>'
								+ value.kategori.nama
								+ '</td>'
								+ '<td> <a href="edit_barang.html?id='
								+ value.id
								+ '" class="btn btn-sm btn-warning" title="Edit"> <i class="far fa-edit"></i></a> </td>'
								+ '<td> <a href="#" class="btn btn-sm btn-danger" title="Hapus" onclick="hapus('
								+ value.id
								+ ')"><i class="fas fa-trash-alt"></i></a> </td>'
								+ '</tr>';
					});
	$("table tbody").append(row);
}
function hapus(id, _this) {
	if (confirm('Yakin menghapus data barang ?') == true) {
		$.ajax({
			url : "http://localhost:8080/deletebarang?id=" + id,
			type : "GET",
			headers : {
				'token' : token
			},
			success : function(data) {
				if (data.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Hapus barang berhasil");
					window.location = "barang.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(data.errorMessage);
					console.log("error", data);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
			}
		})
	} else {
		event.preventDefault();
	}
}