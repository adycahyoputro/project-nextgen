var object = "";
//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);
$(document).ready(function() {
	$.ajax({
		url : "http://localhost:8080/getallpengguna",
		type : "GET",
		data : {

		},
		success : function(data) {
			object = data;
			if (data.returnCode == 0) {
				getAllPengguna();
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(object.errorMessage);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
		}
	})
})
function getAllPengguna() {
	var row = [];
	jQuery
			.each(
					object.pengguna,
					function(index, value) {
						row += '<tr>'
								+ '<td>'
								+ value.id
								+ '</td>'
								+ '<td>'
								+ value.nama
								+ '</td>'
								+ '<td>'
								+ value.tanggalBuat
								+ '</td>'
								+ '<td>'
								+ value.tanggalEdit
								+ '</td>'
								+ '<td>'
								+ value.kategoriPengguna.nama
								+ '</td>'
								+ '<td> <a href="edit_pengguna.html?id='
								+ value.id
								+ '" class="btn btn-sm btn-warning" title="Edit"> <i class="far fa-edit"></i></a> </td>'
								+ '<td> <a href="#" class="btn btn-sm btn-danger" title="Hapus" onclick="hapus('
								+ value.id
								+ ')"><i class="fas fa-trash-alt"></i></a> </td>'
								+ '</tr>';
					});
	$("table tbody").append(row);
}
function hapus(id, _this) {
	var answer = confirm('Yakin menghapus data pengguna ?');
	if (answer) {
		$.ajax({
			url : "http://localhost:8080/deletepengguna?id=" + id,
			type : "GET",
			headers : {
				'token' : token
			},
			success : function(data) {
				if (data.returnCode == 0) {
					//$("#alert").attr("style", "display: block;");
					//$("#alert").html("Hapus pengguna berhasil");
					window.location = "pengguna.html";
				} else {
					$("#alert").attr("style", "display: block;");
					$("#alert").html(data.errorMessage);
				}
			},
			error : function(e) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(e.responseText);
			}
		})
	} else {
		event.preventDefault();
	}
}