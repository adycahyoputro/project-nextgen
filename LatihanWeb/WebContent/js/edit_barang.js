		//var token = getCookie("token");
		var token = sessionStorage.getItem("text", token);

		$(document).ready(function() {
			getKategoriBarang();
			$("#submit").click(function() {
				if ($("#kodeBarang").val() == "") {
					$("#alert").attr("style", "display: block;");
					$("#alert").text("Kode barang tidak boleh kosong");
					$("#kodeBarang").focus();
					return false;
				}
				if ($("#nama").val() == "") {
					$("#alert").attr("style", "display: block;");
					$("#alert").text("Nama barang tidak boleh kosong");
					$("#nama").focus();
					return false;
				}
				if ($("#satuan").val() == "") {
					$("#alert").attr("style", "display: block;");
					$("#alert").text("Satuan tidak boleh kosong");
					$("#satuan").focus();
					return false;
				}
				if ($("#kategori").val() == "") {
					$("#alert").attr("style", "display: block;");
					$("#alert").text("Kategori barang harus dipilih");
					$("#kategori").focus();
					return false;
				}

				var barang = {
						kodeBarang : $("#kodeBarang").val(),
						satuan : $("#satuan").val(),
						hargaBeli : 0,
						hargaJual : 0,
						stok : 0,
						kategori : {
								id : $("#kategori option:selected").val()
							},
						id : $("#id").val(),
						nama : $("#nama").val()
					};

				$.ajax({
					url : "http://localhost:8080/editbarang",
					contentType : "application/json",
					type : "POST",
					headers : {
						'token' : token
					},
					data : JSON.stringify(barang),
					success : function(result) {
						if (result.returnCode == 0) {
							$("#alert").attr("style", "display: block;");
							$("#alert").html("Edit barang berhasil");
						} else {
							$("#alert").attr("style", "display: block;");
							$("#alert").html(result.errorMessage);
						}
					},
					error : function(e) {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(e.responseText);
					}
				})
			})
		})
		function getKategoriBarang() {
			$.ajax({
	    		url: "http://localhost:8080/getallkategoribarang",
				type:"GET",
				data: {

				},
				success: function (data) {
					if (data.returnCode == 0) {
						console.log("success : ", data);
						var option = "<option value=''>Pilih Kategori Barang</option>";
						jQuery.each(data.kategoriBarang, function(key, value) {
							option += "<option value="+value.id+">"+value.nama+"</option>";
						});
						$("#kategori").append(option);
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(data.errorMessage);
					}
					getBarangById();
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
		}
		function getBarangById() {
			const queryString = window.location.search;
			const urlParams = new URLSearchParams(queryString);
			const id = urlParams.get('id');
			$.ajax({
	    		url: "http://localhost:8080/getbarang?id="+id,
				type:"GET",
				data: {

				},
				success: function (data) {
					if (data.returnCode == 0) {
						console.log("success : ", data);
						$("label[for='id']").text("ID Barang : "+data.barang.id);
						$("#id").val(data.barang.id);
						$("#kodeBarang").val(data.barang.kodeBarang);
						$("#tanggalDibuat").val(data.barang.tanggalBuat);
						$("#tanggalDiedit").val(data.barang.tanggalEdit);
						$("#nama").val(data.barang.nama);
						$("#satuan").val(data.barang.satuan);
						$("select[name='kategori']").find("option[value='"+data.barang.kategori.id+"']").attr("selected",true).change();
					} else {
						$("#alert").attr("style", "display: block;");
						$("#alert").html(data.errorMessage);
					}
	        	},
	        	error: function (e) {
	        		$("#alert").attr("style", "display: block;");
					$("#alert").html(e.responseText);
		        }
			 })
		}