//var token = getCookie("token");
var token = sessionStorage.getItem("text", token);
var object = "";
$(document).ready(function() {
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const id = urlParams.get('id');
	$.ajax({
		url : "http://localhost:8080/getkategoribarang?id=" + id,
		type : "GET",
		success : function(data) {
			object = data;
			if (object.returnCode == 0) {
				getKategoriBarang();
				console.log("success :", object);
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(object.errorMessage);
				console.log("error", data);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
			console.log("error", e.responseText);
		}
	})
	$("#submit").click(function() {
		if ($("#nama").val() == "") {
			$("#alert").attr("style", "display: block;");
			$("#alert").text("Nama kategori barang tidak boleh kosong");
			$("#nama").focus();
			return false;
		}
		var edit = {};
		edit["id"] = $("#id").val();
		edit["nama"] = $("#nama").val();
		var textJson = JSON.stringify(edit);
		postEditKategoriBarang(textJson);
	})
})
function getKategoriBarang() {
	var kategoribarang = object.kategoriBarang;
	$("label[for='id']").text("ID Kategori Barang : " + kategoribarang.id);
	$("#id").val(kategoribarang.id);
	$("#nama").val(kategoribarang.nama);
	$("#tanggalBuat").val(kategoribarang.tanggalBuat);
	$("#tanggalEdit").val(kategoribarang.tanggalEdit);
}
function postEditKategoriBarang(Json) {
	$.ajax({
		url : "http://localhost:8080/editkategoribarang",
		contentType : "application/json",
		type : "POST",
		headers : {
			'token' : token
		},
		data : Json,
		success : function(result) {
			if (result.returnCode == 0) {
				$("#alert").attr("style", "display: block;");
				$("#alert").html("Edit kategori barang berhasil");
			} else {
				$("#alert").attr("style", "display: block;");
				$("#alert").html(result.errorMessage);
			}
		},
		error : function(e) {
			$("#alert").attr("style", "display: block;");
			$("#alert").html(e.responseText);
		}
	})
}