package com.nextgen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "retur")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Retur extends BaseEntity{
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Barang barang;
	@Column(nullable = false)
	private int hargaJual;
	@Column(nullable = false)
	private int jumlah;
	private String keterangan;
	
	public Barang getBarang() {
		return barang;
	}
	public void setBarang(Barang barang) {
		this.barang = barang;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	

}
