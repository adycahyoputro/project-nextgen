package com.nextgen.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pembelian")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pembelian extends BaseEntity{
	
	@Column(nullable = false)
	private String noFaktur;
	private Date tanggalBeli;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Supplier supplier;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Barang barang;
	@Column(nullable = false)
	private int hargaPokok;
	@Column(nullable = false)
	private int hargaJual;
	@Column(nullable = false)
	private int jumlah;
	
	public String getNoFaktur() {
		return noFaktur;
	}
	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}
	public Date getTanggalBeli() {
		return tanggalBeli;
	}
	public void setTanggalBeli(Date tanggalBeli) {
		this.tanggalBeli = tanggalBeli;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Barang getBarang() {
		return barang;
	}
	public void setBarang(Barang barang) {
		this.barang = barang;
	}
	public int getHargaPokok() {
		return hargaPokok;
	}
	public void setHargaPokok(int hargaPokok) {
		this.hargaPokok = hargaPokok;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	
	

}
