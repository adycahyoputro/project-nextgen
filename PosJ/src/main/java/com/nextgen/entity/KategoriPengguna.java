package com.nextgen.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "kategori_pengguna")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class KategoriPengguna extends BaseEntity{

}
