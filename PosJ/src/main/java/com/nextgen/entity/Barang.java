package com.nextgen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "barang")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Barang extends BaseEntity{
	
	@Column(nullable = false, length = 255)
	private String kodeBarang;
	@Column(nullable = false, length = 255)
	private String satuan;
	@Column(nullable = false)
	private int hargaBeli;
	@Column(nullable = false)
	private int hargaJual;
	@Column(nullable = false)
	private int stok;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private KategoriBarang kategori;
	
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public int getHargaBeli() {
		return hargaBeli;
	}
	public void setHargaBeli(int hargaBeli) {
		this.hargaBeli = hargaBeli;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getStok() {
		return stok;
	}
	public void setStok(int stok) {
		this.stok = stok;
	}
	public KategoriBarang getKategori() {
		return kategori;
	}
	public void setKategori(KategoriBarang kategori) {
		this.kategori = kategori;
	}
	
	

}
