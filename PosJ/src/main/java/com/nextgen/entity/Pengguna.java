package com.nextgen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pengguna")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pengguna extends BaseEntity{
	
	@Column(nullable = false, length = 255)
	private String password;
	private String token;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private KategoriPengguna kategoriPengguna;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public KategoriPengguna getKategoriPengguna() {
		return kategoriPengguna;
	}
	public void setKategoriPengguna(KategoriPengguna kategoriPengguna) {
		this.kategoriPengguna = kategoriPengguna;
	}
	
	

}
