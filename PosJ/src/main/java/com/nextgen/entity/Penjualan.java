package com.nextgen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "penjualan")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Penjualan extends BaseEntity{
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Barang barang;
	@Column(nullable = false)
	private int hargaJual;
	@Column(nullable = false)
	private int hargaBeli;
	@Column(nullable = false)
	private int diskon;
	@Column(nullable = false)
	private int jumlah;
	@Column(nullable = false)
	private int subTotal;
	@Column(nullable = false)
	private int laba;
	
	public Barang getBarang() {
		return barang;
	}
	public void setBarang(Barang barang) {
		this.barang = barang;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getHargaBeli() {
		return hargaBeli;
	}
	public void setHargaBeli(int hargaBeli) {
		this.hargaBeli = hargaBeli;
	}
	public int getDiskon() {
		return diskon;
	}
	public void setDiskon(int diskon) {
		this.diskon = diskon;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	public int getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}
	public int getLaba() {
		return laba;
	}
	public void setLaba(int laba) {
		this.laba = laba;
	}
	
	

}
