package com.nextgen.api.bean.transaction;

public class TrBarangBean {
	
	private int id;
	private String kodeBarang;
	private String namaBarang;
	private String satuan;
	private int hargaPokok;
	private int hargaEceran;
	private int hargaGrosir;
	private int stok;
	private int minStok;
	private String kategori;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public int getHargaPokok() {
		return hargaPokok;
	}
	public void setHargaPokok(int hargaPokok) {
		this.hargaPokok = hargaPokok;
	}
	public int getHargaEceran() {
		return hargaEceran;
	}
	public void setHargaEceran(int hargaEceran) {
		this.hargaEceran = hargaEceran;
	}
	public int getHargaGrosir() {
		return hargaGrosir;
	}
	public void setHargaGrosir(int hargaGrosir) {
		this.hargaGrosir = hargaGrosir;
	}
	public int getStok() {
		return stok;
	}
	public void setStok(int stok) {
		this.stok = stok;
	}
	public int getMinStok() {
		return minStok;
	}
	public void setMinStok(int minStok) {
		this.minStok = minStok;
	}
	public String getKategori() {
		return kategori;
	}
	public void setKategori(String kategori) {
		this.kategori = kategori;
	}
	
	

}
