package com.nextgen.api.bean;

import java.util.List;

import com.nextgen.entity.Barang;

public class BarangListResponse extends BaseResponse{
	
	private List<Barang> barang;

	public List<Barang> getBarang() {
		return barang;
	}

	public void setBarang(List<Barang> barang) {
		this.barang = barang;
	}
	
	

}
