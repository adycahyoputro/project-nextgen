package com.nextgen.api.bean.transaction;

import java.util.List;

public class TrPenjualanRequest {
	
	private int totalBelanja;
	private int tunai;
	private int kembalian;
	private List<TrJualBarangBean> barang;
	
	public int getTotalBelanja() {
		return totalBelanja;
	}
	public void setTotalBelanja(int totalBelanja) {
		this.totalBelanja = totalBelanja;
	}
	public int getTunai() {
		return tunai;
	}
	public void setTunai(int tunai) {
		this.tunai = tunai;
	}
	public int getKembalian() {
		return kembalian;
	}
	public void setKembalian(int kembalian) {
		this.kembalian = kembalian;
	}
	public List<TrJualBarangBean> getBarang() {
		return barang;
	}
	public void setBarang(List<TrJualBarangBean> barang) {
		this.barang = barang;
	}
	
	

}
