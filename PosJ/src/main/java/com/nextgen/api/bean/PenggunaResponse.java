package com.nextgen.api.bean;

import com.nextgen.entity.Pengguna;

public class PenggunaResponse extends BaseResponse{
	
	private Pengguna pengguna;

	public Pengguna getPengguna() {
		return pengguna;
	}

	public void setPengguna(Pengguna pengguna) {
		this.pengguna = pengguna;
	}
	
	

}
