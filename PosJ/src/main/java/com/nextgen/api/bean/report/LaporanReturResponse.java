package com.nextgen.api.bean.report;

import java.util.List;

import com.nextgen.api.bean.BaseResponse;

public class LaporanReturResponse extends BaseResponse{
	
	private List<LaporanReturBean> retur;

	public List<LaporanReturBean> getRetur() {
		return retur;
	}

	public void setRetur(List<LaporanReturBean> retur) {
		this.retur = retur;
	}
	
	

}
