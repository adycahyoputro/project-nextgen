package com.nextgen.api.bean;

import com.nextgen.entity.KategoriBarang;

public class KategoriBarangResponse extends BaseResponse{
	
	private KategoriBarang kategoriBarang;

	public KategoriBarang getKategoriBarang() {
		return kategoriBarang;
	}

	public void setKategoriBarang(KategoriBarang kategoriBarang) {
		this.kategoriBarang = kategoriBarang;
	}
	
	

}
