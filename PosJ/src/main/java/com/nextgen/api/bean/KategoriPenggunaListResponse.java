package com.nextgen.api.bean;

import java.util.List;

import com.nextgen.entity.KategoriPengguna;

public class KategoriPenggunaListResponse extends BaseResponse{
	
	private List<KategoriPengguna> kategoriPengguna;

	public List<KategoriPengguna> getKategoriPengguna() {
		return kategoriPengguna;
	}

	public void setKategoriPengguna(List<KategoriPengguna> kategoriPengguna) {
		this.kategoriPengguna = kategoriPengguna;
	}
	
	

}
