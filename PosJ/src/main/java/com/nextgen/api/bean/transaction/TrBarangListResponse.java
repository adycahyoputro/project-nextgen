package com.nextgen.api.bean.transaction;

import java.util.List;

import com.nextgen.api.bean.BaseResponse;

public class TrBarangListResponse extends BaseResponse{

	private List<TrBarangBean> barang;

	public List<TrBarangBean> getBarang() {
		return barang;
	}

	public void setBarang(List<TrBarangBean> barang) {
		this.barang = barang;
	}
	
	
}
