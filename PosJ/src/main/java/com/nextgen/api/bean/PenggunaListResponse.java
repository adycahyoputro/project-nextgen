package com.nextgen.api.bean;

import java.util.List;

import com.nextgen.entity.Pengguna;

public class PenggunaListResponse extends BaseResponse{
	
	private List<Pengguna> pengguna;

	public List<Pengguna> getPengguna() {
		return pengguna;
	}

	public void setPengguna(List<Pengguna> pengguna) {
		this.pengguna = pengguna;
	}
	
	

}
