package com.nextgen.api.bean;

import com.nextgen.interfaces.IErrorCode;

public class BaseResponse {

	private int returnCode = IErrorCode.NO_ERROR;
	
	private String errorMessage = "";

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
