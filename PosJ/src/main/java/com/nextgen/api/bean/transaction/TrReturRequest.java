package com.nextgen.api.bean.transaction;

import java.util.List;

public class TrReturRequest {
	
	private List<TrReturBarangBean> barang;

	public List<TrReturBarangBean> getBarang() {
		return barang;
	}

	public void setBarang(List<TrReturBarangBean> barang) {
		this.barang = barang;
	}
	
	

}
