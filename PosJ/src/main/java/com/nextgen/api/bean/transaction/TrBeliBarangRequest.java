package com.nextgen.api.bean.transaction;

import java.util.List;

public class TrBeliBarangRequest {
	
	private int supplier;
	private String nomorFaktur;
	private String tanggal;
	private List<TrBeliBarangBean> barang;
	
	public int getSupplier() {
		return supplier;
	}
	public void setSupplier(int supplier) {
		this.supplier = supplier;
	}
	public String getNomorFaktur() {
		return nomorFaktur;
	}
	public void setNomorFaktur(String nomorFaktur) {
		this.nomorFaktur = nomorFaktur;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public List<TrBeliBarangBean> getBarang() {
		return barang;
	}
	public void setBarang(List<TrBeliBarangBean> barang) {
		this.barang = barang;
	}
	
	

}
