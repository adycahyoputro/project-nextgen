package com.nextgen.api.bean;

import java.util.List;

import com.nextgen.entity.Supplier;

public class SupplierListResponse extends BaseResponse{

	private List<Supplier> supplier;

	public List<Supplier> getSupplier() {
		return supplier;
	}

	public void setSupplier(List<Supplier> supplier) {
		this.supplier = supplier;
	}
	
	
}
