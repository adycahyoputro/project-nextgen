package com.nextgen.api.bean.report;

public class LaporanPenjualanBean {
	
	private String tanggal;
	private String barang;
	private int hargaBeli;
	
	private int hargaJual;
	private int diskon;
	private int hargaJualNet;
	private int jumlah;
	private int totalPenjualan;
	private int totalLaba;
	
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getBarang() {
		return barang;
	}
	public void setBarang(String barang) {
		this.barang = barang;
	}
	public int getHargaBeli() {
		return hargaBeli;
	}
	public void setHargaBeli(int hargaBeli) {
		this.hargaBeli = hargaBeli;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getDiskon() {
		return diskon;
	}
	public void setDiskon(int diskon) {
		this.diskon = diskon;
	}
	public int getHargaJualNet() {
		return hargaJualNet;
	}
	public void setHargaJualNet(int hargaJualNet) {
		this.hargaJualNet = hargaJualNet;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	public int getTotalPenjualan() {
		return totalPenjualan;
	}
	public void setTotalPenjualan(int totalPenjualan) {
		this.totalPenjualan = totalPenjualan;
	}
	public int getTotalLaba() {
		return totalLaba;
	}
	public void setTotalLaba(int totalLaba) {
		this.totalLaba = totalLaba;
	}
	
	

}
