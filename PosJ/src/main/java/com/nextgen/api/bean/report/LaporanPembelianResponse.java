package com.nextgen.api.bean.report;

import java.util.List;

import com.nextgen.api.bean.BaseResponse;

public class LaporanPembelianResponse extends BaseResponse{
	
	private List<LaporanPembelianBean> pembelian;

	public List<LaporanPembelianBean> getPembelian() {
		return pembelian;
	}

	public void setPembelian(List<LaporanPembelianBean> pembelian) {
		this.pembelian = pembelian;
	}
	
	

}
