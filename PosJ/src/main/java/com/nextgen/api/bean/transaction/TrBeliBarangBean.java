package com.nextgen.api.bean.transaction;

public class TrBeliBarangBean {
	
	private int id;
	private String kodeBarang;
	private int hargaPokok;
	private int hargaJual;
	private int jumlah;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public int getHargaPokok() {
		return hargaPokok;
	}
	public void setHargaPokok(int hargaPokok) {
		this.hargaPokok = hargaPokok;
	}
	public int getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(int hargaJual) {
		this.hargaJual = hargaJual;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	
	

}
