package com.nextgen.api.bean;

import com.nextgen.entity.Supplier;

public class SupplierResponse extends BaseResponse{
	
	private Supplier supplier;

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	

}
