package com.nextgen.api.bean;

import com.nextgen.entity.Barang;

public class BarangResponse extends BaseResponse{
	
	private Barang barang;

	public Barang getBarang() {
		return barang;
	}

	public void setBarang(Barang barang) {
		this.barang = barang;
	}
	
	

}
