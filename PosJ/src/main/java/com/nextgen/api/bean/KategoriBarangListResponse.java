package com.nextgen.api.bean;

import java.util.List;

import com.nextgen.entity.KategoriBarang;

public class KategoriBarangListResponse extends BaseResponse{
	
	private List<KategoriBarang> kategoriBarang;

	public List<KategoriBarang> getKategoriBarang() {
		return kategoriBarang;
	}

	public void setKategoriBarang(List<KategoriBarang> kategoriBarang) {
		this.kategoriBarang = kategoriBarang;
	}
	
	

}
