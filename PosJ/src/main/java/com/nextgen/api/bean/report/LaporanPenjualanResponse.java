package com.nextgen.api.bean.report;

import java.util.List;

import com.nextgen.api.bean.BaseResponse;

public class LaporanPenjualanResponse extends BaseResponse{
	
	private List<LaporanPenjualanBean> penjualan;

	public List<LaporanPenjualanBean> getPenjualan() {
		return penjualan;
	}

	public void setPenjualan(List<LaporanPenjualanBean> penjualan) {
		this.penjualan = penjualan;
	}
	
	

}
