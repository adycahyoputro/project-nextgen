package com.nextgen.api.bean.transaction;

import java.util.List;

import com.nextgen.api.bean.BaseResponse;

public class TrSupplierListResponse extends BaseResponse{
	
	private List<TrSupplierBean> supplier;

	public List<TrSupplierBean> getSupplier() {
		return supplier;
	}

	public void setSupplier(List<TrSupplierBean> supplier) {
		this.supplier = supplier;
	}
	
	

}
