package com.nextgen.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.BaseResponse;
import com.nextgen.api.bean.KategoriBarangListResponse;
import com.nextgen.api.bean.KategoriBarangResponse;
import com.nextgen.entity.KategoriBarang;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.KategoriBarangService;

@CrossOrigin(origins = "*")
@RestController
public class KategoriBarangApiController {
	
	@Autowired
	private KategoriBarangService kategoriBarangService;
	
	@GetMapping(value = "/getallkategoribarang")
	public KategoriBarangListResponse getAllKategoriBarang() {
		KategoriBarangListResponse ret = new KategoriBarangListResponse();
		
		try {
			ret.setKategoriBarang(kategoriBarangService.getAllKategoriBarang());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/getkategoribarang")
	public KategoriBarangResponse getKategoriBarang(@RequestParam int id) {
		KategoriBarangResponse ret = new KategoriBarangResponse();
		
		try {
			ret.setKategoriBarang(kategoriBarangService.getKategoriBarangById(id));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/addkategoribarang")
	public BaseResponse addKategoriBarang(@RequestHeader String token, @RequestBody KategoriBarang kategoriBarang) {
		BaseResponse ret = new BaseResponse();
		
		try {
			kategoriBarangService.addKategoriBarang(token, kategoriBarang.getNama());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/editkategoribarang")
	public BaseResponse editKategoriBarang(@RequestHeader String token, @RequestBody KategoriBarang kategoriBarang) {
		BaseResponse ret = new BaseResponse();
		
		try {
			kategoriBarangService.editKategoriBarang(token, kategoriBarang.getId(), kategoriBarang.getNama());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/deletekategoribarang")
	public BaseResponse deleteKategoriBarang(@RequestHeader String token, @RequestParam int id) {
		BaseResponse ret = new BaseResponse();
		
		try {
			kategoriBarangService.deleteKategoriBarang(token, id);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

}
