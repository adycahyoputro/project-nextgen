package com.nextgen.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.report.LaporanPembelianResponse;
import com.nextgen.api.bean.report.LaporanPenjualanResponse;
import com.nextgen.api.bean.report.LaporanReturResponse;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.LaporanService;

@CrossOrigin(origins = "*")
@RestController
public class LaporanApiController {
	
	@Autowired
	private LaporanService laporanService;
	
	@GetMapping(value = "/laporan/pembelian")
	public LaporanPembelianResponse laporanPembelian(@RequestHeader String token, @RequestParam String start, @RequestParam String end) {
		LaporanPembelianResponse ret = new LaporanPembelianResponse();
		
		try {
			ret.setPembelian(laporanService.getLaporanPembelian(token, start, end));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/laporan/penjualan")
	public LaporanPenjualanResponse laporanPenjualan(@RequestHeader String token, @RequestParam String start, @RequestParam String end) {
		LaporanPenjualanResponse ret = new LaporanPenjualanResponse();
		
		try {
			ret.setPenjualan(laporanService.getLaporanPenjualan(token, start, end));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/laporan/retur")
	public LaporanReturResponse laporanRetur(@RequestHeader String token, @RequestParam String start, @RequestParam String end) {
		LaporanReturResponse ret = new LaporanReturResponse();
		
		try {
			ret.setRetur(laporanService.getLaporanRetur(token, start, end));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

}
