package com.nextgen.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.BaseResponse;
import com.nextgen.api.bean.SupplierListResponse;
import com.nextgen.api.bean.SupplierResponse;
import com.nextgen.entity.Supplier;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.SupplierService;

@CrossOrigin(origins = "*")
@RestController
public class SupplierApiController {
	
	@Autowired
	private SupplierService supplierService;
	
	@GetMapping(value = "/getallsupplier")
	public SupplierListResponse getAllSupplier() {
		SupplierListResponse ret = new SupplierListResponse();
		
		try {
			ret.setSupplier(supplierService.getAllSupplier());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/getsupplier")
	public SupplierResponse getSupplier(@RequestParam int id) {
		SupplierResponse ret = new SupplierResponse();
		
		try {
			ret.setSupplier(supplierService.getSupplierById(id));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/addsupplier")
	public BaseResponse addSupplier(@RequestHeader String token, @RequestBody Supplier supplier) {
		BaseResponse ret = new BaseResponse();
		
		try {
			supplierService.addSupplier(token, supplier.getNama(), supplier.getAlamat(), supplier.getNoTelp());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/editsupplier")
	public BaseResponse editSupplier(@RequestHeader String token, @RequestBody Supplier supplier) {
		BaseResponse ret = new BaseResponse();
		
		try {
			supplierService.editSupplier(token, supplier.getId(), supplier.getNama(), supplier.getAlamat(), supplier.getNoTelp());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/deletesupplier")
	public BaseResponse deleteSupplier(@RequestHeader String token, @RequestParam int id) {
		BaseResponse ret = new BaseResponse();
		
		try {
			supplierService.deleteSupplier(token, id);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

}
