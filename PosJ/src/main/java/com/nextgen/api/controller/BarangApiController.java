package com.nextgen.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.BarangListResponse;
import com.nextgen.api.bean.BarangResponse;
import com.nextgen.api.bean.BaseResponse;
import com.nextgen.entity.Barang;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.BarangService;

@CrossOrigin(origins = "*")
@RestController
public class BarangApiController {
	
	@Autowired
	private BarangService barangService;
	
	@GetMapping(value = "/getallbarang")
	public BarangListResponse getAllBarang() {
		BarangListResponse ret = new BarangListResponse();
		
		try {
			ret.setBarang(barangService.getAllBarang());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/getbarang")
	public BarangResponse getBarang(@RequestParam int id) {
		BarangResponse ret = new BarangResponse();
		
		try {
			ret.setBarang(barangService.getBarangById(id));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/addbarang")
	public BaseResponse addBarang(@RequestHeader String token, @RequestBody Barang barang) {
		BaseResponse ret = new BaseResponse();
		
		try {
			barangService.addBarang(token, barang.getNama(), barang.getKodeBarang(), barang.getSatuan(), barang.getKategori().getId());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/editbarang")
	public BaseResponse editBarang(@RequestHeader String token, @RequestBody Barang barang) {
		BaseResponse ret = new BaseResponse();
		
		try {
			barangService.editBarang(token, barang.getId(), barang.getNama(), barang.getKodeBarang(), barang.getSatuan(), barang.getKategori().getId());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/deletebarang")
	public BaseResponse deleteBarang(@RequestHeader String token, @RequestParam int id) {
		BaseResponse ret = new BaseResponse();
		
		try {
			barangService.deleteBarang(token, id);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

}
