package com.nextgen.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.BaseResponse;
import com.nextgen.api.bean.transaction.TrBarangListResponse;
import com.nextgen.api.bean.transaction.TrBeliBarangRequest;
import com.nextgen.api.bean.transaction.TrPenjualanRequest;
import com.nextgen.api.bean.transaction.TrReturRequest;
import com.nextgen.api.bean.transaction.TrSupplierListResponse;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.TransactionService;

@CrossOrigin(origins = "*")
@RestController
public class TransactionApiController {
	
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping(value = "/tr/getallsupplier")
	public TrSupplierListResponse getAllSupplier() {
		TrSupplierListResponse ret = new TrSupplierListResponse();
		
		try {
			ret.setSupplier(transactionService.getAllSupplier());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/tr/getallbarang")
	public TrBarangListResponse getAllBarang() {
		TrBarangListResponse ret = new TrBarangListResponse();
		
		try {
			ret.setBarang(transactionService.getAllBarang());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/tr/postbelibarang")
	public BaseResponse postBeliBarang(@RequestHeader String token, @RequestBody TrBeliBarangRequest req) {
		BaseResponse ret = new BaseResponse();
		
		try {
			transactionService.beliBarang(token, req.getSupplier(), req.getNomorFaktur(), req.getTanggal(), req.getBarang());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/tr/postpenjualan")
	public BaseResponse postPenjualan(@RequestHeader String token, @RequestBody TrPenjualanRequest req) {
		BaseResponse ret = new BaseResponse();
		
		try {
			transactionService.postPenjualan(token, req.getTotalBelanja(), req.getTunai(), req.getKembalian(), req.getBarang());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/tr/postretur")
	public BaseResponse postRetur(@RequestHeader String token, @RequestBody TrReturRequest req) {
		BaseResponse ret = new BaseResponse();
		
		try {
			transactionService.postRetur(token, req.getBarang());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

}
