package com.nextgen.api.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextgen.api.bean.BaseResponse;
import com.nextgen.api.bean.KategoriPenggunaListResponse;
import com.nextgen.api.bean.PenggunaListResponse;
import com.nextgen.api.bean.PenggunaResponse;
import com.nextgen.entity.Pengguna;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;
import com.nextgen.service.PenggunaService;

@CrossOrigin(origins = "*")
@RestController
//@RequestMapping("")
public class PenggunaApiController {
	
	@Autowired
	private PenggunaService penggunaService;
	
	@GetMapping(value = "/getallpengguna")
	public PenggunaListResponse getAllPengguna() {
		PenggunaListResponse ret = new PenggunaListResponse();
		
		try {
			ret.setPengguna(penggunaService.getAllPengguna());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/getpengguna")
	public PenggunaResponse getPengguna(@RequestParam int id) {
		PenggunaResponse ret = new PenggunaResponse();
		
		try {
			ret.setPengguna(penggunaService.getPenggunaById(id));
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/addpengguna")
	public BaseResponse addPengguna(@RequestHeader String token, @RequestBody Pengguna pengguna) {
		BaseResponse ret = new BaseResponse();
		
		try {
			penggunaService.addPengguna(token, pengguna.getNama(), pengguna.getPassword(), pengguna.getKategoriPengguna().getId());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@PostMapping(value = "/editpengguna")
	public BaseResponse editPengguna(@RequestHeader String token, @RequestBody Pengguna pengguna) {
		BaseResponse ret = new BaseResponse();
		
		try {
			penggunaService.editPengguna(token, pengguna.getId(), pengguna.getPassword(), pengguna.getKategoriPengguna().getId());
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalaha teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/deletepengguna")
	public BaseResponse deletePengguna(@RequestHeader String token, @RequestParam int id) {
		BaseResponse ret = new BaseResponse();
		
		try {
			penggunaService.deletePengguna(token, id);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "/getallkategoripengguna")
	public KategoriPenggunaListResponse getAllKategoriPengguna() {
		KategoriPenggunaListResponse ret = new KategoriPenggunaListResponse();
		
		try {
			ret.setKategoriPengguna(penggunaService.getAllKategoriPengguna());
		} catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}

	@PostMapping(value = "login_api")
	public PenggunaResponse login(HttpSession session, @RequestBody Pengguna pengguna) {
		PenggunaResponse ret = new PenggunaResponse();
		
		try {
			Pengguna user = penggunaService.login(pengguna.getNama(), pengguna.getPassword());
			session.setAttribute("token", user.getToken());
			ret.setPengguna(user);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
	
	@GetMapping(value = "logout_api")
	public BaseResponse logout(@RequestHeader String token) {
		BaseResponse ret = new BaseResponse();
		
		try {
			penggunaService.logout(token);
		} catch (ApplicationException e) {
			// TODO: handle exception
			ret.setErrorMessage(e.getMessage());
			ret.setReturnCode(e.getErrorCode());
		}catch (Exception e) {
			// TODO: handle exception
			ret.setErrorMessage("Terjadi kesalahan teknis");
			ret.setReturnCode(IErrorCode.ERROR_GENERAL);
		}
		return ret;
	}
}
