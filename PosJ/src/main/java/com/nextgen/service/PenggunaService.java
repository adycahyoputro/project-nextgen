package com.nextgen.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.dao.KategoriPenggunaDao;
import com.nextgen.dao.PenggunaDao;
import com.nextgen.entity.KategoriPengguna;
import com.nextgen.entity.Pengguna;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class PenggunaService {
	
	@Autowired
	private PenggunaDao usersDao;
	
	@Autowired
	private KategoriPenggunaDao kategoriPenggunaDao;
	
	public Pengguna login(String username, String password)throws ApplicationException{
		Log.log("");
		Pengguna user = usersDao.findPenggunaByUserName(username);
		
		if(user == null) {
			throw new ApplicationException("Email pengguna tidak ditemukan", IErrorCode.ERROR_LOGIN_USER_NOT_FOUND);
		}
		String hashPassword = encodePassword(password).toString();
		
		if(!user.getPassword().equals(hashPassword)) {
			throw new ApplicationException("Password tidak valid", IErrorCode.ERROR_LOGIN_PASSWORD_INVALID);
		}
		String random = System.currentTimeMillis()+"";
		int ret = usersDao.updateToken(user.getId(), random);
		
		if(ret != 1) {
			throw new ApplicationException("Login update token error", IErrorCode.ERROR_UPDATE_ENTITY);
		}
		user.setToken(random);
//		user.setPassword("");
		return user;
	}
	
	public void logout(String token)throws ApplicationException{
		Log.log("");
		Pengguna user = usersDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token pengguna tidak ditemukan", IErrorCode.ERROR_TOKEN_NOT_FOUND);
		}
		String random = System.currentTimeMillis() + "";
		int ret = usersDao.updateToken(user.getId(), random);
		
		if(ret != 1) {
			throw new ApplicationException("Logout update token error", IErrorCode.ERROR_UPDATE_ENTITY);
		}
	}
	
	public List<Pengguna> getAllPengguna(){
		Log.log("");
		List<Pengguna>listPengguna = usersDao.findAll();
		for(Iterator iterator = listPengguna.iterator(); iterator.hasNext();) {
			Pengguna pengguna = (Pengguna) iterator.next();
			//pengguna.setPassword("");
		}
		return listPengguna;
	}
	
	public List<KategoriPengguna> getAllKategoriPengguna(){
		Log.log("");
		List<KategoriPengguna> listKategoriPengguna = kategoriPenggunaDao.findAll();
		return listKategoriPengguna;
	}
	
	public Pengguna getPenggunaById(int id)throws ApplicationException{
		Log.log("");
		Pengguna pengguna = usersDao.findById(id);
		if(pengguna == null) {
			throw new ApplicationException("Pengguna tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		//pengguna.setPassword("");
		return pengguna;
	}
	
	public void addPengguna(String token, String nama, String password, int kategoriPengguna) throws ApplicationException{
		Log.log("");
		Pengguna user = usersDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		StringBuilder sb = encodePassword(password);
		password = sb.toString();
		Pengguna pengguna = usersDao.findPenggunaByUserName(nama);
		
		if(pengguna != null) {
			throw new ApplicationException("Nama pengguna sudah terpakai", IErrorCode.ERROR_DUPLICATE_ENTITY);
		}
		
		KategoriPengguna kategoriPng = kategoriPenggunaDao.findKategoriPenggunaById(kategoriPengguna);
		
		if(kategoriPng == null) {
			throw new ApplicationException("Kategori pengguna tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		usersDao.addPengguna(nama, new Date(), password, kategoriPng);
	}
	
	public void editPengguna(String token, int id, String password, int kategoriPengguna)throws ApplicationException{
		Log.log("");
		Pengguna user = usersDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalEdit = new Date();
		StringBuilder sb = encodePassword(password);
		password = sb.toString();
		KategoriPengguna kategoriPng = kategoriPenggunaDao.findKategoriPenggunaById(kategoriPengguna);
		
		if(kategoriPng == null) {
			throw new ApplicationException("Kategori pengguna tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		int ret = usersDao.editPengguna(id, tanggalEdit, password, kategoriPng);
		
		if(ret != 1) {
			throw new ApplicationException("Update data pengguna tidak berhasil", IErrorCode.ERROR_UPDATE_ENTITY);
		}
	}
	
	public void deletePengguna(String token, int id)throws ApplicationException{
		Log.log("");
		Pengguna user = usersDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		
		if(id == 1) {
			throw new ApplicationException("Super admin tidak bisa dihapus", IErrorCode.ERROR_DELETE_ENTITY);
		}
		int ret = usersDao.deletePengguna(id);
		
		if(ret != 1) {
			throw new ApplicationException("Hapus data pengguna tidak berhasil", IErrorCode.ERROR_DELETE_ENTITY);
		}
	}
	
	private StringBuilder encodePassword(String password) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
		
		StringBuilder sb = new StringBuilder();
		
		for(byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		
		return sb;
	}

}
