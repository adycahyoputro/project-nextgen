package com.nextgen.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.api.bean.transaction.TrBarangBean;
import com.nextgen.api.bean.transaction.TrBeliBarangBean;
import com.nextgen.api.bean.transaction.TrJualBarangBean;
import com.nextgen.api.bean.transaction.TrReturBarangBean;
import com.nextgen.api.bean.transaction.TrSupplierBean;
import com.nextgen.dao.BarangDao;
import com.nextgen.dao.PembelianDao;
import com.nextgen.dao.PenggunaDao;
import com.nextgen.dao.PenjualanDao;
import com.nextgen.dao.ReturDao;
import com.nextgen.dao.SupplierDao;
import com.nextgen.entity.Barang;
import com.nextgen.entity.Pengguna;
import com.nextgen.entity.Supplier;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class TransactionService {
	
	@Autowired
	private PembelianDao pembelianDao;
	@Autowired
	private BarangDao barangDao;
	@Autowired
	private SupplierDao supplierDao;
	@Autowired
	private PenggunaDao userDao;
	@Autowired
	private PenjualanDao penjualanDao;
	@Autowired
	private ReturDao returDao;
	
	public List<TrBarangBean> getAllBarang(){
		Log.log("");
		List<TrBarangBean> ret = new ArrayList<TrBarangBean>();
		List<Barang> barang = barangDao.findAll();
		for(Iterator iterator = barang.iterator(); iterator.hasNext();) {
			Barang b = (Barang) iterator.next();
			TrBarangBean barangBean = new TrBarangBean();
			barangBean.setId(b.getId());
			barangBean.setHargaEceran(b.getHargaJual());
			barangBean.setHargaGrosir(b.getHargaJual());
			barangBean.setHargaPokok(b.getHargaBeli());
			barangBean.setKategori(b.getKategori().getNama());
			barangBean.setMinStok(0);
			barangBean.setNamaBarang(b.getNama());
			barangBean.setSatuan(b.getSatuan());
			barangBean.setStok(b.getStok());
			barangBean.setKodeBarang(b.getKodeBarang());
			
			ret.add(barangBean);
		}
		return ret;
	}
	
	public List<TrSupplierBean> getAllSupplier(){
		Log.log("");
		List<TrSupplierBean> ret = new ArrayList<TrSupplierBean>();
		List<Supplier> supplier = supplierDao.findAll();
		for(Iterator iterator = supplier.iterator(); iterator.hasNext();) {
			Supplier s = (Supplier) iterator.next();
			TrSupplierBean supplierBean = new TrSupplierBean();
			supplierBean.setId(s.getId());
			supplierBean.setAlamat(s.getAlamat());
			supplierBean.setNama(s.getNama());
			supplierBean.setNoTelp(s.getNoTelp());
			ret.add(supplierBean);
		}
		return ret;
	}
	
	public void beliBarang(String token, int supplier, String nomorfaktur, String tanggal, List<TrBeliBarangBean> barang)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		System.out.println(token);
		Supplier supplierE = supplierDao.findById(supplier);
		
		if(supplierE == null) {
			throw new ApplicationException("Supplier tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		for(TrBeliBarangBean trbarang : barang) {
			Barang barangE = barangDao.findById(trbarang.getId());
			if(barangE == null) {
				throw new ApplicationException("Barang tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
			}
			
			int hargaPokok = trbarang.getHargaPokok();
			int hargaJual = trbarang.getHargaJual();
			int jumlah = trbarang.getJumlah();
			
			int stokAwal = barangE.getStok();
			int stokAkhir = stokAwal + jumlah;
			int hargaBeliAwal = barangE.getHargaBeli();
			int hargaBeliAkhir = ((hargaBeliAwal * stokAwal) + (hargaPokok * jumlah)) / stokAkhir;
			
			Date date = null;
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(tanggal);
			} catch (Exception e) {
				// TODO: handle exception
				throw new ApplicationException("Format tanggal tidak tepat", IErrorCode.ERROR_GENERAL);
			}
			Date edit = new Date();
			
			pembelianDao.postBeliBarang(supplierE, nomorfaktur, date, barangE, hargaPokok, hargaJual, jumlah, edit);
			int ret = barangDao.updateBarang(stokAkhir, edit, hargaJual, hargaBeliAkhir, barangE.getId());
			if(ret != 1) {
				throw new ApplicationException("Update barang tidak berhasil, id = " + barangE.getId(), IErrorCode.ERROR_UPDATE_ENTITY);
			}
		}
	}
	
	public void postPenjualan(String token, int total, int tunai, int kembalian, List<TrJualBarangBean> barang)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		for(TrJualBarangBean penjualan : barang) {
			Barang barangE = barangDao.findById(penjualan.getId());
			if(barangE == null) {
				throw new ApplicationException("Barang tidak ditemukan, id = " + penjualan.getId(), IErrorCode.ERROR_ENTITY_NOT_FOUND);
			}
			int hargaJual = barangE.getHargaJual();
			int hargaBeli = barangE.getHargaBeli();
			
			int diskon = penjualan.getDiskon();
			int jumlah = penjualan.getJumlah();
			int subTotal = penjualan.getSubTotal();
			int laba = (hargaJual - diskon - hargaBeli) * jumlah;
			
			int stok = barangE.getStok();
			int sisa = stok - jumlah;
			if(sisa < 0) {
				throw new ApplicationException("Stock barang tidak mencukupi, id = " + barangE.getId(), IErrorCode.ERROR_STOCK);
			}
			
			Date tanggal = new Date();
			penjualanDao.postPenjualan(barangE, hargaJual, diskon, jumlah, hargaBeli, subTotal, laba, tanggal);
			int ret = barangDao.updateStok(sisa, tanggal, barangE.getId());
			if(ret != 1) {
				throw new ApplicationException("Update stock barang tidak berhasil, id = " + barangE.getId(), IErrorCode.ERROR_UPDATE_ENTITY);
			}
		}
	}
	
	public void postRetur(String token, List<TrReturBarangBean> barang)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		for(TrReturBarangBean retur : barang) {
			Barang barangE = barangDao.findById(retur.getId());
			if(barangE == null) {
				throw new ApplicationException("Barang tidak ditemukan, id = " + retur.getId(), IErrorCode.ERROR_ENTITY_NOT_FOUND);
			}
			String tanggal = retur.getTanggal();
			Date date = null;
			
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(tanggal);
			} catch (Exception e) {
				// TODO: handle exception
				throw new ApplicationException("Format tanggal tidak tepat", IErrorCode.ERROR_GENERAL);
			}
			int hargaJual = retur.getHargaJual();
			int jumlah = retur.getJumlah();
			String keterangan = retur.getKeterangan();
			returDao.postRetur(date, barangE, hargaJual, jumlah, keterangan);
		}
	}

}
