package com.nextgen.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.dao.BarangDao;
import com.nextgen.dao.KategoriBarangDao;
import com.nextgen.dao.PenggunaDao;
import com.nextgen.entity.Barang;
import com.nextgen.entity.KategoriBarang;
import com.nextgen.entity.Pengguna;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class BarangService {

	@Autowired
	private BarangDao barangDao;
	
	@Autowired
	private KategoriBarangDao kategoriBarangDao;
	
	@Autowired
	private PenggunaDao userDao;
	
	public List<Barang> getAllBarang(){
		Log.log("");
		List<Barang> listBarang = barangDao.findAll();
		return listBarang;
	}
	
	public Barang getBarangById(int id)throws ApplicationException{
		Log.log("");
		Barang barang = barangDao.findById(id);
		
		if(barang == null) {
			throw new ApplicationException("Barang tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		return barang;
	}
	
	public void addBarang(String token, String nama, String kodeBarang, String satuan, int kategori)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalBuat = new Date();
		KategoriBarang ktgBarang = kategoriBarangDao.findById(kategori);
		
		if(ktgBarang == null) {
			throw new ApplicationException("Kategori barang tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		barangDao.addBarang(nama, tanggalBuat, kodeBarang, satuan, ktgBarang);
	}
	
	public void editBarang(String token, int id, String nama, String kodeBarang, String satuan, int kategori)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalEdit = new Date();
		KategoriBarang kategoriBarang = kategoriBarangDao.findById(kategori);
		
		if(kategoriBarang == null) {
			throw new ApplicationException("Kategori barang tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		int ret = barangDao.editBarang(id, nama, tanggalEdit, kodeBarang, satuan, kategoriBarang);
		
		if(ret != 1) {
			throw new ApplicationException("Update barang tidak berhasil", IErrorCode.ERROR_UPDATE_ENTITY);
		}
	}
	
	public void deleteBarang(String token, int id)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		int ret = barangDao.deleteBarang(id);
		
		if(ret != 1) {
			throw new ApplicationException("Hapus data barang tidak berhasil", IErrorCode.ERROR_DELETE_ENTITY);
		}
	}
}
