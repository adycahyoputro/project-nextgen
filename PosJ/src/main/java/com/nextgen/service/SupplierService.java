package com.nextgen.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.dao.PenggunaDao;
import com.nextgen.dao.SupplierDao;
import com.nextgen.entity.Pengguna;
import com.nextgen.entity.Supplier;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class SupplierService {

	@Autowired
	private SupplierDao supplierDao;
	
	@Autowired
	private PenggunaDao userDao;
	
	public void addSupplier(String token, String nama, String alamat, String noTelp)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalBuat = new Date();
		supplierDao.addSupplier(nama, tanggalBuat, alamat, noTelp);
	}
	
	public List<Supplier> getAllSupplier(){
		Log.log("");
		List<Supplier> listSupplier = supplierDao.findAll();
		return listSupplier;
	}
	
	public Supplier getSupplierById(int id)throws ApplicationException{
		Log.log("");
		Supplier supplier = supplierDao.findById(id);
		
		if(supplier == null) {
			throw new ApplicationException("Supplier tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		return supplier;
	}
	
	public void editSupplier(String token, int id, String nama, String alamat, String noTelp)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalEdit = new Date();
		int ret = supplierDao.editSupplier(id, nama, tanggalEdit, alamat, noTelp);
		
		if(ret != 1) {
			throw new ApplicationException("Update supplier tidak berhasil", IErrorCode.ERROR_UPDATE_ENTITY);
		}
		
	}
	
	public void deleteSupplier(String token, int id)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		int ret = supplierDao.deleteSupplier(id);
		
		if(ret != 1) {
			throw new ApplicationException("Hapus data supplier tidak berhasil", IErrorCode.ERROR_DELETE_ENTITY);
		}
	}
}
