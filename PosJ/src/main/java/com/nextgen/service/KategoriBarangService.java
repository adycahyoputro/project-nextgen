package com.nextgen.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.dao.KategoriBarangDao;
import com.nextgen.dao.PenggunaDao;
import com.nextgen.entity.KategoriBarang;
import com.nextgen.entity.Pengguna;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class KategoriBarangService {
	
	@Autowired
	private KategoriBarangDao kategoriBarangDao;
	
	@Autowired
	private PenggunaDao userDao;
	
	public List<KategoriBarang> getAllKategoriBarang(){
		Log.log("");
		List<KategoriBarang> listKategori = kategoriBarangDao.findAll();
		return listKategori;
	}
	
	public KategoriBarang getKategoriBarangById(int id) throws ApplicationException{
		Log.log("");
		KategoriBarang kategoriBarang = kategoriBarangDao.findById(id);
		
		if(kategoriBarang == null) {
			throw new ApplicationException("Kategori barang tidak ditemukan", IErrorCode.ERROR_ENTITY_NOT_FOUND);
		}
		return kategoriBarang;
	}
	
	public void addKategoriBarang(String token, String nama) throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalBuat = new Date();
		kategoriBarangDao.addKategoriBarang(nama, tanggalBuat);
	}
	
	public void editKategoriBarang(String token, int id, String nama) throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date tanggalEdit = new Date();
		int ret = kategoriBarangDao.updateKategoriBarang(id, nama, tanggalEdit);
		
		if(ret != 1) {
			throw new ApplicationException("Update kategori barang tidak berhasil", IErrorCode.ERROR_UPDATE_ENTITY);
		}
	}
	
	public void deleteKategoriBarang(String token, int id) throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		int ret = kategoriBarangDao.deleteKategoriBarang(id);
		
		if(ret != 1) {
			throw new ApplicationException("Hapus data kategori barang tidak berhasil", IErrorCode.ERROR_DELETE_ENTITY);
		}
	}

}
