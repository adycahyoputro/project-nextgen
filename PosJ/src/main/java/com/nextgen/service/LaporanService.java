package com.nextgen.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nextgen.api.bean.report.LaporanPembelianBean;
import com.nextgen.api.bean.report.LaporanPenjualanBean;
import com.nextgen.api.bean.report.LaporanReturBean;
import com.nextgen.dao.LaporanDao;
import com.nextgen.dao.PenggunaDao;
import com.nextgen.entity.Pengguna;
import com.nextgen.exception.ApplicationException;
import com.nextgen.interfaces.IErrorCode;

import log.Log;

@Service
@Transactional
public class LaporanService {

	@Autowired
	private PenggunaDao userDao;
	
	@Autowired
	private LaporanDao laporanDao;
	
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public List<LaporanPembelianBean> getLaporanPembelian(String token, String start, String end)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date dstart = null;
		Date dend = null;
		
		try {
			dstart = df.parse(start);
			dend = df.parse(end);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ApplicationException("Format tanggal tidak tepat", IErrorCode.ERROR_GENERAL);
		}
		
		List<LaporanPembelianBean> ret = new ArrayList<LaporanPembelianBean>();
		List<Object[]> laporan = laporanDao.findPembelian(dstart, dend);
		
		for(Iterator iterator = laporan.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			
			LaporanPembelianBean pembelian = new LaporanPembelianBean();
			pembelian.setBarang((String)objects[1]);
			pembelian.setSupplier((String)objects[2]);
			pembelian.setNomorFaktur((String)objects[3]);
			pembelian.setHargaBeli((int)objects[4]);
			pembelian.setHargaJual((int)objects[5]);
			pembelian.setJumlah((int)objects[6]);
			pembelian.setSubTotal(pembelian.getHargaBeli()*pembelian.getJumlah());
			ret.add(pembelian);
		}
		return ret;
	}
	
	public List<LaporanPenjualanBean> getLaporanPenjualan(String token, String start, String end)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date dstart = null;
		Date dend = null;
		
		try {
			dstart = df.parse(start);
			dend = df.parse(end);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ApplicationException("Format tanggal tidak tepat", IErrorCode.ERROR_GENERAL);
		}
		
		List<LaporanPenjualanBean> ret = new ArrayList<LaporanPenjualanBean>();
		List<Object[]> laporan = laporanDao.findPenjualan(dstart, dend);
		
		for(Iterator iterator = laporan.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			
			LaporanPenjualanBean penjualan = new LaporanPenjualanBean();
			penjualan.setTanggal(df.format((Date)objects[0]));
			penjualan.setBarang((String)objects[1]);
			penjualan.setHargaBeli((int)objects[2]);
			penjualan.setHargaJual((int)objects[3]);
			penjualan.setDiskon((int)objects[4]);
			penjualan.setHargaJualNet(penjualan.getHargaJual() - penjualan.getDiskon());
			penjualan.setJumlah((int)objects[5]);
			penjualan.setTotalPenjualan((int)objects[6]);
			penjualan.setTotalLaba((int)objects[7]);
			ret.add(penjualan);
		}
		return ret;
		
	}
	
	public List<LaporanReturBean> getLaporanRetur(String token, String start, String end)throws ApplicationException{
		Log.log("");
		Pengguna user = userDao.findPenggunaByToken(token);
		
		if(user == null) {
			throw new ApplicationException("Token tidak valid", IErrorCode.ERROR_INVALID_TOKEN);
		}
		Date dstart = null;
		Date dend = null;
		
		try {
			dstart = df.parse(start);
			dend = df.parse(end);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ApplicationException("Format tanggal tidak tepat", IErrorCode.ERROR_GENERAL);
		}
		List<LaporanReturBean> ret = new ArrayList<LaporanReturBean>();
		List<Object[]> laporan = laporanDao.findRetur(dstart, dend);
		
		for(Iterator iterator = laporan.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			LaporanReturBean retur = new LaporanReturBean();
			retur.setTanggal(df.format((Date)objects[0]));
			retur.setBarang((String)objects[1]);
			retur.setKeterangan((String)objects[2]);
			retur.setHargaJual((int)objects[3]);
			retur.setJumlah((int)objects[4]);
			retur.setSubTotal(retur.getHargaJual() * retur.getJumlah());
			ret.add(retur);
		}
		return ret;
	}
}
