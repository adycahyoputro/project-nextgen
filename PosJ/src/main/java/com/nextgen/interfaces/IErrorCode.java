package com.nextgen.interfaces;

public interface IErrorCode {
	
	public static final int NO_ERROR = 0;
	
	public static final int ERROR_LOGIN_USER_NOT_FOUND = 1;
	
	public static final int ERROR_LOGIN_PASSWORD_INVALID = 2;
	
	public static final int ERROR_ENTITY_NOT_FOUND = 3;
	
	public static final int ERROR_DUPLICATE_ENTITY = 4;
	
	public static final int ERROR_UPDATE_ENTITY = 5;
	
	public static final int ERROR_DELETE_ENTITY = 6;
	
	public static final int ERROR_STOCK = 7;
	
	public static final int ERROR_TOKEN_NOT_FOUND = 8;
	
	public static final int ERROR_INVALID_TOKEN = 9;

	public static final int ERROR_GENERAL = -99;
}
