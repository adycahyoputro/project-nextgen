package com.nextgen.dao;

import java.util.Date;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.Barang;
import com.nextgen.entity.Retur;

import log.Log;

@Repository
public class ReturDao {
	
	private EntityManager em;
	
	public ReturDao (EntityManager em) {
		this.em = em;
	}
	
	public void postRetur(Date tanggal, Barang barang, int hargaJual, int jumlah, String keterangan) {
		Log.log("");
		Retur returEntity = new Retur();
		returEntity.setNama("");
		returEntity.setTanggalBuat(tanggal);
		returEntity.setBarang(barang);
		returEntity.setJumlah(jumlah);
		returEntity.setHargaJual(hargaJual);
		returEntity.setKeterangan(keterangan);
		
		em.persist(returEntity);
	}

}
