package com.nextgen.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.KategoriBarang;

import log.Log;

@Repository
public class KategoriBarangDao {
	
	private EntityManager em;
	
	public KategoriBarangDao (EntityManager em) {
		this.em = em;
	}
	
	public List<KategoriBarang> findAll(){
		Log.log("");
		Query q = em.createQuery("SELECT k FROM KategoriBarang k");
		List<KategoriBarang> ret = q.getResultList();
		return ret;
	}
	
	public KategoriBarang findById(int id) {
		Log.log("");
		Query q = em.createQuery("SELECT k FROM KategoriBarang k WHERE k.id = :id ");
		q.setParameter("id", id);
		KategoriBarang ret = null;
		try {
			ret = (KategoriBarang) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public int updateKategoriBarang(int id, String nama, Date tanggalEdit) {
		Log.log("");
		Query q = em.createQuery("UPDATE KategoriBarang k SET k.nama = :nama, k.tanggalEdit = :tanggalEdit WHERE k.id = :id ");
		q.setParameter("nama", nama);
		q.setParameter("tanggalEdit", tanggalEdit);
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
	public void addKategoriBarang(String nama, Date tanggalBuat) {
		Log.log("");
		KategoriBarang kategori = new KategoriBarang();
		kategori.setNama(nama);
		kategori.setTanggalBuat(tanggalBuat);
		
		em.persist(kategori);
	}
	
	public int deleteKategoriBarang(int id) {
		Log.log("");
		Query q = em.createQuery("DELETE FROM KategoriBarang k WHERE k.id = :id ");
		q.setParameter("id", id);
		return q.executeUpdate();
	}

}
