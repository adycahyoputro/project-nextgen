package com.nextgen.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.Supplier;

import log.Log;

@Repository
public class SupplierDao {

	private EntityManager em;
	
	public SupplierDao(EntityManager em) {
		this.em = em;
	}
	
	public List<Supplier> findAll(){
		Log.log("");
		Query q = em.createQuery("SELECT s FROM Supplier s");
		List<Supplier> ret = q.getResultList();
		return ret;
	}
	
	public void addSupplier(String nama, Date tanggalBuat, String alamat, String noTelp) {
		Log.log("");
		Supplier supplier = new Supplier();
		supplier.setNama(nama);
		supplier.setTanggalBuat(tanggalBuat);
		supplier.setAlamat(alamat);
		supplier.setNoTelp(noTelp);
		
		em.persist(supplier);
	}
	
	public Supplier findById(int id) {
		Log.log("");
		Query q = em.createQuery("SELECT s FROM Supplier s WHERE s.id = :id");
		q.setParameter("id", id);
		Supplier ret = null;
		try {
			ret = (Supplier) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public int editSupplier(int id, String nama, Date tanggalEdit, String alamat, String noTelp) {
		Log.log("");
		Query q = em.createQuery("UPDATE Supplier s SET s.nama = :nama, s.tanggalEdit = :tanggalEdit, s.alamat = :alamat, s.noTelp = :noTelp WHERE s.id = :id");
		q.setParameter("nama", nama);
		q.setParameter("tanggalEdit", tanggalEdit);
		q.setParameter("alamat", alamat);
		q.setParameter("noTelp", noTelp);
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
	public int deleteSupplier(int id) {
		Log.log("");
		Query q = em.createQuery("DELETE FROM Supplier s WHERE s.id = :id");
		q.setParameter("id", id);
		return q.executeUpdate();
	}
}
