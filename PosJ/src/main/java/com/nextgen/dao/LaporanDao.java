package com.nextgen.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import log.Log;

@Repository
public class LaporanDao {

	private EntityManager em;
	
	public LaporanDao (EntityManager em) {
		this.em = em;
	}
	
	public List<Object[]> findPembelian(Date start, Date end){
		Log.log("");
		String sql = "SELECT p.tanggalBeli, b.nama, s.nama, p.noFaktur, p.hargaPokok, p.hargaJual, p.jumlah "
				+"FROM Pembelian p LEFT JOIN p.barang b LEFT JOIN p.supplier s WHERE p.tanggalBeli BETWEEN :start AND :end ORDER BY p.id";
		Query query = em.createQuery(sql);
		query.setParameter("start", start);
		query.setParameter("end", end);
		return query.getResultList();
	}
	
	public List<Object[]> findPenjualan(Date start, Date end){
		Log.log("");
		String sql = "SELECT p.tanggalBuat, b.nama, p.hargaBeli, p.hargaJual, p.diskon, p.jumlah, p.subTotal, p.laba "
				+"FROM Penjualan p LEFT JOIN p.barang b WHERE p.tanggalBuat BETWEEN :start AND :end ORDER BY p.id";
		Query query = em.createQuery(sql);
		query.setParameter("start", start);
		query.setParameter("end", end);
		return query.getResultList();
	}
	
	public List<Object[]> findRetur(Date start, Date end){
		Log.log("");
		String sql = "SELECT p.tanggalBuat, b.nama, p.keterangan, p.hargaJual, p.jumlah FROM Retur p LEFT JOIN p.barang b WHERE p.tanggalBuat BETWEEN :start AND :end ORDER BY p.id";
		Query query = em.createQuery(sql);
		query.setParameter("start", start);
		query.setParameter("end", end);
		return query.getResultList();
	}
}
