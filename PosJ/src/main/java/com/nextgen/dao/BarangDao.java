package com.nextgen.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.Barang;
import com.nextgen.entity.KategoriBarang;

import log.Log;

@Repository
public class BarangDao {
	
	private EntityManager em;
	
	public BarangDao(EntityManager em) {
		this.em = em;
	}
	
	public List<Barang> findAll(){
		Log.log("");
		Query q = em.createQuery("SELECT b FROM Barang b");
		List<Barang> ret = q.getResultList();
		return ret;
	}
	
	public Barang findById(int id) {
		Log.log("");
		Query q = em.createQuery("SELECT b FROM Barang b WHERE b.id = :id");
		q.setParameter("id", id);
		Barang ret = null;
		try {
			ret = (Barang) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public Barang findByKodeBarang(String kodeBarang) {
		Log.log("");
		Query q = em.createQuery("SELECT b FROM Barang b WHERE b.kodeBarang = :kodeBarang");
		q.setParameter("kodeBarang", kodeBarang);
		Barang ret = null;
		try {
			ret = (Barang) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public void addBarang(String nama, Date tanggalBuat, String kodeBarang, String satuan, KategoriBarang kategoriBarang) {
		Log.log("");
		Barang barang = new Barang();
		barang.setNama(nama);
		barang.setTanggalBuat(tanggalBuat);
		barang.setTanggalEdit(tanggalBuat);
		barang.setKodeBarang(kodeBarang);
		barang.setSatuan(satuan);
		barang.setHargaBeli(0);
		barang.setHargaJual(0);
		barang.setStok(0);
		barang.setKategori(kategoriBarang);
		
		em.persist(barang);
	}
	
	public int editBarang(int id, String nama, Date tanggalEdit, String kodeBarang, String satuan, KategoriBarang kategoriBarang) {
		Log.log("");
		Query q = em.createQuery("UPDATE Barang b SET b.nama = :nama, b.tanggalEdit = :tanggalEdit, b.kodeBarang = :kodeBarang, b.satuan = :satuan, b.kategori = :kategori WHERE b.id = :id ");
		q.setParameter("nama", nama);
		q.setParameter("tanggalEdit", tanggalEdit);
		q.setParameter("kodeBarang", kodeBarang);
		q.setParameter("satuan", satuan);
		q.setParameter("kategori", kategoriBarang);
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
	public int deleteBarang(int id) {
		Log.log("");
		Query q = em.createQuery("DELETE FROM Barang b WHERE b.id = :id ");
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
	public int updateBarang(int stokAkhir, Date tanggal, int hargaJual, int hargaBeliAkhir, int id) {
		Log.log("");
		Query q = em.createQuery("UPDATE Barang b SET b.stok = :stok, b.tanggalEdit = :tanggal, b.hargaJual = :hargaJual, b.hargaBeli = :hargaBeliAkhir WHERE b.id = :id");
		q.setParameter("stok", stokAkhir);
		q.setParameter("tanggal", tanggal);
		q.setParameter("hargaJual", hargaJual);
		q.setParameter("hargaBeliAkhir", hargaBeliAkhir);
		q.setParameter("id", id);
		return q.executeUpdate();
	}
	
	public int updateStok(int sisa, Date tanggalEdit, int id) {
		Log.log("");
		Query q = em.createQuery("UPDATE Barang b SET b.stok = :stok, b.tanggalEdit = :tanggalEdit WHERE b.id = :id");
		q.setParameter("stok", sisa);
		q.setParameter("tanggalEdit", tanggalEdit);
		q.setParameter("id", id);
		return q.executeUpdate();
	}

}
