package com.nextgen.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.KategoriPengguna;
import com.nextgen.entity.Pengguna;


import log.Log;

@Repository
public class PenggunaDao {
	
	private EntityManager em;
	
	public PenggunaDao(EntityManager em) {
		this.em = em;
	}
	
	public Pengguna findPenggunaByUserName(String username) {
		Log.log("");
		Query q = em.createQuery("FROM Pengguna u WHERE u.nama = :username");
		q.setParameter("username", username);
		Pengguna ret = null;
		try {
			ret = (Pengguna) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		return ret;
	}
	
	public Pengguna findPenggunaByToken(String token) {
		Log.log("");
		Query q = em.createQuery("FROM Pengguna u WHERE u.token = :token");
		q.setParameter("token", token);
		Pengguna ret = null;
		try {
			ret = (Pengguna) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public List<Pengguna> findAll(){
		Log.log("");
		Query q = em.createQuery("SELECT p FROM Pengguna p");
		List<Pengguna> listPengguna = q.getResultList();
		return listPengguna;
	}
	
	public Pengguna findById(int id) {
		Log.log("");
		em.getEntityManagerFactory().getCache().evictAll();
		Query q = em.createQuery("SELECT p FROM Pengguna p WHERE p.id = :id");
		q.setParameter("id", id);
		Pengguna ret = null;
		try {
			ret = (Pengguna) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}
	
	public void addPengguna(String username, Date tanggalBuat, String hashPassword, KategoriPengguna kategoriPengguna) {
		Log.log("");
		Pengguna pengguna = new Pengguna();
		pengguna.setNama(username);
		pengguna.setTanggalBuat(tanggalBuat);
		pengguna.setTanggalEdit(null);
		pengguna.setPassword(hashPassword);
		pengguna.setKategoriPengguna(kategoriPengguna);
		
		em.persist(pengguna);
		em.flush();
	}
	
	public int editPengguna(int id, Date tanggalEdit, String password, KategoriPengguna kategoriPengguna) {
		Log.log("");
		Query q = em.createQuery("UPDATE Pengguna p SET p.tanggalEdit = :tanggalEdit, "
				+"p.password = :password, p.kategoriPengguna = :kategoriPengguna "
				+"WHERE p.id = :id");
		q.setParameter("tanggalEdit", tanggalEdit);
		q.setParameter("password", password);
		q.setParameter("kategoriPengguna", kategoriPengguna);
		q.setParameter("id", id);
		int ret = q.executeUpdate();
		em.flush();
		return ret;
	}
	
	public int deletePengguna(int id) {
		Log.log("");
		Query q = em.createQuery("DELETE FROM Pengguna p WHERE p.id = :id");
		q.setParameter("id", id);
		int ret = q.executeUpdate();
		em.flush();
		return ret;
	}
	
	public int updateToken(int id, String token) {
		Log.log("");
		Query q = em.createQuery("UPDATE Pengguna p SET p.token = :token WHERE p.id = :id");
		q.setParameter("token", token);
		q.setParameter("id", id);
		int ret = q.executeUpdate();
		em.flush();
		return ret;
	}

}
