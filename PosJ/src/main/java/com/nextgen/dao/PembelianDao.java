package com.nextgen.dao;

import java.util.Date;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.Barang;
import com.nextgen.entity.Pembelian;
import com.nextgen.entity.Supplier;

import log.Log;

@Repository
public class PembelianDao {

	private EntityManager em;
	
	public PembelianDao(EntityManager em) {
		this.em = em;
	}
	
	public void postBeliBarang(Supplier supplier, String nomorFaktur, Date tanggalBeli, Barang barang, int hargaBeli, int hargaJual, int stok, Date tanggal) {
		Log.log("");
		Pembelian pembelian = new Pembelian();
		pembelian.setNama("");
		pembelian.setNoFaktur(nomorFaktur);
		pembelian.setSupplier(supplier);
		pembelian.setBarang(barang);
		pembelian.setHargaPokok(hargaBeli);
		pembelian.setHargaJual(hargaJual);
		pembelian.setJumlah(stok);
		pembelian.setTanggalBeli(tanggalBeli);
		pembelian.setTanggalBuat(tanggal);
		
		em.persist(pembelian);
	}
}
