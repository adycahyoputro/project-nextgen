package com.nextgen.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nextgen.entity.KategoriPengguna;

import log.Log;

@Repository
public class KategoriPenggunaDao {
	
	private EntityManager em;
	
	public KategoriPenggunaDao (EntityManager em) {
		this.em = em;
	}
	
	public List<KategoriPengguna> findAll(){
		Log.log("");
		Query q = em.createQuery("SELECT k FROM KategoriPengguna k");
		List<KategoriPengguna> listKategoriPengguna = q.getResultList();
		return listKategoriPengguna;
		
	}
	
	public KategoriPengguna findKategoriPenggunaById(int id) {
		Log.log("");
		Query q = em.createQuery("FROM KategoriPengguna u WHERE u.id = :id");
		q.setParameter("id", id);
		KategoriPengguna ret = null;
		try {
			ret = (KategoriPengguna) q.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ret;
	}

}
